const express = require('express');

const app = express();

app.get('/', (req, res) => {
    res.send('node application')
})

app.listen(3000,'0.0.0.0',()=>{
    console.log('listening on port 3000');
})